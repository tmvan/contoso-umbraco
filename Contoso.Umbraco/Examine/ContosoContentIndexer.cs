﻿using Examine;
using Lucene.Net.Analysis;
using Lucene.Net.Index;
using System.Collections.Generic;
using Umbraco.Core;
using Umbraco.Core.Services;
using UmbracoExamine;

namespace Contoso.Umbraco.Examine
{
    public class ContosoContentIndexer : UmbracoContentIndexer
    {
        private readonly IContentService _contentService;

        public ContosoContentIndexer(
            IIndexCriteria indexerData,
            IndexWriter writer,
            UmbracoExamine.DataServices.IDataService dataService,
            IContentService contentService,
            IMediaService mediaService,
            IDataTypeService dataTypeService,
            IUserService userService,
            IContentTypeService contentTypeService,
            bool async)
            : base(indexerData, writer, dataService, contentService, mediaService, dataTypeService, userService, contentTypeService, async)
        {
            this._contentService = contentService;
        }

        public ContosoContentIndexer(
            IIndexCriteria indexerData,
            Lucene.Net.Store.Directory luceneDirectory,
            UmbracoExamine.DataServices.IDataService dataService,
            IContentService contentService,
            IMediaService mediaService,
            IDataTypeService dataTypeService,
            IUserService userService,
            IContentTypeService contentTypeService,
            Analyzer analyzer,
            bool async) : base(indexerData, luceneDirectory, dataService, contentService, mediaService, dataTypeService, userService, contentTypeService, analyzer, async)
        {
            this._contentService = contentService;
        }

        public ContosoContentIndexer()
        {
            this._contentService = ApplicationContext.Current.Services.ContentService;
        }

        protected override Dictionary<string, string> GetSpecialFieldsToIndex(Dictionary<string, string> allValuesForIndexing)
        {
            var fields = base.GetSpecialFieldsToIndex(allValuesForIndexing);

            var content = this._contentService.GetById(int.Parse(allValuesForIndexing["id"]));

            if (content != null)
            {
                var joinedString = string.Join("|", content.ContentType.CompositionAliases());
                fields.Add("__BaseTypes", $"|{joinedString}|");
            }

            return fields;
        }
    }
}