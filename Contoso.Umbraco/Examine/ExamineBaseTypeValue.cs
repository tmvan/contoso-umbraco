﻿using Examine.SearchCriteria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Umbraco.Examine
{
    public class ExamineBaseTypeValue : IExamineValue
    {
        public Examineness Examineness => Examineness.Fuzzy;

        public float Level => 0;

        public string Value { get; private set; }

        public ExamineBaseTypeValue(string value)
        {
            this.Value = $"|{value}|";
        }
    }
}
