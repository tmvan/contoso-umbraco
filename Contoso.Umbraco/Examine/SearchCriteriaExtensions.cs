﻿using Examine.SearchCriteria;

namespace Contoso.Umbraco.Examine
{
    public static class SearchCriteriaExtensions
    {
        public static IBooleanOperation IsContent(this IQuery query)
        {
            return query.Field("__IndexType", "content");
        }

        public static IBooleanOperation IsComposedOf(this IQuery query, string documentTypeAlias)
        {
            return query.Field("__BaseTypes", new ExamineBaseTypeValue(documentTypeAlias));
        }

        public static IBooleanOperation IsHideOnMenu(this IQuery query)
        {
            return query.Field("hideOnMenu", "1");
        }
    }
}