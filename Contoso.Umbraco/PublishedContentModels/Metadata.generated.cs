//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.7.99
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Contoso.Umbraco.PublishedContentModels
{
	// Mixin content Type 1069 with alias "metadata"
	/// <summary>Metadata</summary>
	public partial interface IMetadata : IPublishedContent
	{
		/// <summary>Meta Description</summary>
		string MetaDescription { get; }

		/// <summary>Meta Image</summary>
		IPublishedContent MetaImage { get; }

		/// <summary>Meta Keywords</summary>
		IEnumerable<string> MetaKeywords { get; }

		/// <summary>Meta Title</summary>
		string MetaTitle { get; }
	}

	/// <summary>Metadata</summary>
	[PublishedContentModel("metadata")]
	public partial class Metadata : PublishedContentModel, IMetadata
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "metadata";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Metadata(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Metadata, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Meta Description
		///</summary>
		[ImplementPropertyType("metaDescription")]
		public string MetaDescription
		{
			get { return GetMetaDescription(this); }
		}

		/// <summary>Static getter for Meta Description</summary>
		public static string GetMetaDescription(IMetadata that) { return that.GetPropertyValue<string>("metaDescription"); }

		///<summary>
		/// Meta Image
		///</summary>
		[ImplementPropertyType("metaImage")]
		public IPublishedContent MetaImage
		{
			get { return GetMetaImage(this); }
		}

		/// <summary>Static getter for Meta Image</summary>
		public static IPublishedContent GetMetaImage(IMetadata that) { return that.GetPropertyValue<IPublishedContent>("metaImage"); }

		///<summary>
		/// Meta Keywords
		///</summary>
		[ImplementPropertyType("metaKeywords")]
		public IEnumerable<string> MetaKeywords
		{
			get { return GetMetaKeywords(this); }
		}

		/// <summary>Static getter for Meta Keywords</summary>
		public static IEnumerable<string> GetMetaKeywords(IMetadata that) { return that.GetPropertyValue<IEnumerable<string>>("metaKeywords"); }

		///<summary>
		/// Meta Title
		///</summary>
		[ImplementPropertyType("metaTitle")]
		public string MetaTitle
		{
			get { return GetMetaTitle(this); }
		}

		/// <summary>Static getter for Meta Title</summary>
		public static string GetMetaTitle(IMetadata that) { return that.GetPropertyValue<string>("metaTitle"); }
	}
}
