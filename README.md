# CONTOSO

Contoso Ltd. on Umbraco

# SETUP

- Duplicate `Contoso.Umbraco\Web.config.default` file and rename it to `Web.config`.
- Duplicate `Contoso.Umbraco\Web.Debug.config.default` file and rename it to `Web.Debug.config`.
- Build project `Contoso.Umbraco` to transform `Web.config` file.
- In `appSettings`, find `umbracoConfigurationStatus` setting and clear its value.
- Find connection string `umbracoDbDSN` and clear its `connectionString` value.
- Run the project `Contoso.Umbraco` or setup a new local IIS website.
- As our expect, we should see the installation page. Just follow the instruction to install Umbraco.
- Once Umbraco is installed. Update your connection string from `Web.config` to `Web.Debug.config` by copying `connectionString` value of connection string `umbracoDbDSN` from `Web.config` to `Web.Debug.config`.
- Build project again and run website.
- We should see the Umbraco empty page noty us that there is no content in our website.
- Go to BackOffice, create `Home` item using `Home` document type. Input some information and publish it.
- Navigate to `Home` in FrontOffice, the homepage should be displayed with metadata, navigation and footer as expected.
- Create `404` using `Editorial` document type. Input content and publish it.
- Go to FrontOffice and navigate to inexistent page, the your 404 should be displayed as expected.